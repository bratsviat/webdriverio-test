const assert = require('assert');
const path = require('path');
const date = new Date;
// const defaults = require('D://webdriverio-test/wdio.conf').config;
// const waitForFileExists = require('../../test/util/waitForFileExists');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const dragAndDrop = require('html-dnd').codeForSelectors;

describe('http://the-internet.herokuapp.com/AB Testing link test', () => {
    it('click on AB Testing link', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=A/B Testing').click();
        const header = $('h3*=A/B Test');
        const content = $('#content > div > p');
        header.waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/abtest', 'Url doesn\'t match');
        assert.notEqual(content.getText().indexOf('Also known as split testing.'), '-1', 'Content text is incorrect');

    });
});

describe('Add/Remove Elements test', () => {
    it('click on add element button', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Add/Remove Elements').click();
        const header = $('h3=Add/Remove Elements');
        const btnAdd = $('button*=Add');
        header.waitForDisplayed();
        btnAdd.click();
        assert.equal($('button.added-manually').isDisplayed(), true, 'Button has not been displayed');
        $('button.added-manually').click();
        assert.equal($('button.added-manually').isDisplayed(), false, 'Button still is displaing');

    });
});

describe('Basic Auth', () => {
    it('Test correct authentication and input cancel', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Basic Auth').click();

        //if authentication window will be canceled
        browser.keys("Escape");
        $('body').waitForDisplayed();
        assert.equal($('body').getText(), 'Not authorized', 'Login and password was accepted');

        //correct login and password input
        browser.url('http://admin:admin@the-internet.herokuapp.com/basic_auth');
        const text = $('h3');
        text.waitForDisplayed();
        assert.equal(text.getText(), 'Basic Auth', 'Login or password is incorrect');
    });
});

describe('Broken images', () => {
    it('Test image loading', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Broken Images').click();
        $('.example').waitForDisplayed();
        let xhr = new XMLHttpRequest();

        for (let i = 2; i <= 4; i++) {
            let url = $('#content > div > img:nth-child(' + i + ')').getAttribute('src');
            xhr.open('GET', url, false);
            xhr.send();
            // assert.equal(xhr.status, '200', 'Picture not found');
        };

    });
});

describe('Challenging DOM', () => {
    it('Find best locators', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Challenging DOM').click();
        $('#content p').waitForExist();
        assert.notEqual($('#content p').getText().indexOf('It\'s more often than not that the application'), '-1', 'Content text is incorrect');
        const blueBtn = $('.button');
        const redBtn = $('.button.alert');
        const greenBtn = $('.button.success');
        let arrBtn = [blueBtn, redBtn, greenBtn];
        blueBtn.isDisplayed();
        redBtn.isDisplayed();
        greenBtn.isDisplayed();

        //link cheking
        const editLink = $('table > tbody > tr:nth-child(4) > td:nth-child(7) > a:nth-child(1)');
        editLink.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/challenging_dom#edit', 'Edit button is not working');
        let delLink = $('table > tbody > tr:nth-child(4) > td:nth-child(7) > a:nth-child(2)');
        delLink.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/challenging_dom#delete', 'Delete button is not working');

        //check canvas answer is changing

        const script = $('#content > script');
        const scriptTextFirst = browser.execute('return arguments[0].innerText', script);
        const indexNumStartFirst = scriptTextFirst.indexOf('Answer');
        const indexNumEndFirst = scriptTextFirst.indexOf('\',90,112);');
        const valueCutFirst = scriptTextFirst.slice(indexNumStartFirst, indexNumEndFirst);

        for (let i = 0; i < arrBtn.length; i++) {
            arrBtn[i].click();
            const script2 = $('#content > script');
            const scriptTextSecond = browser.execute('return arguments[0].innerText', script2);
            const indexNumStartSecond = scriptTextSecond.indexOf('Answer');
            const indexNumEndSecond = scriptTextSecond.indexOf('\',90,112);');
            const valueCutSecond = scriptTextSecond.slice(indexNumStartSecond, indexNumEndSecond);
            assert.notEqual(valueCutFirst, valueCutSecond, 'Value has not changed');
        }

    });
});

describe('Checkboxes', () => {
    it('Test has an element a checked attribute', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Checkboxes').click();
        $('#checkboxes').waitForDisplayed();
        const box1 = $('#checkboxes > :nth-child(1)');
        const box2 = $('#checkboxes > :nth-child(3)');
        box1.click();
        assert.equal(box1.isSelected(), true, 'Checkbox 1 is not selected');
        assert.equal(box2.isSelected(), true, 'Checkbox 2 is not selected');
        box2.click();
        assert.equal(box2.isSelected(), false, 'Checkbox 2 is not selected');
    });
});

describe('Context Menu', () => {
    it('Test right-click in the box to see text alert', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Context Menu').click();
        const context = $('#hot-spot');
        context.waitForDisplayed();
        context.moveTo();
        browser.positionClick(2);
        browser.pause(1000);
        assert.equal(browser.getAlertText(), 'You selected a context menu', 'Menu doesn\'t shows');
        browser.acceptAlert();
    });
});

describe('Disappearing Elements', () => {
    it('Check disappearing element', () => {
        browser.url('http://the-internet.herokuapp.com');
        browser.deleteCookies();
        $('a=Disappearing Elements').click();
        $('#content p').waitForExist();
        const btnList = $('#content > div > ul').getText().split('\n');
        for (let i = 1; i < btnList.length; i++) {
            if ($('li:nth-child(' + i + ') > a').isDisplayed()) {
                assert.equal($('li:nth-child(' + i + ') > a').isDisplayed(), true, 'The element numder ' + i + ' is not displayed');
            } else {
                do {
                    browser.refresh();
                    $('#content p').waitForExist();
                } while ($('li:nth-child(' + i + ') > a').isDisplayed());
            };
        };

    });
});

//Test works when only it starts

// describe('Drag and Drop', () => {
//     it('Test Drag&Drop function', () => {
//         browser.url('http://the-internet.herokuapp.com');
//         $('a=Drag and Drop').click();
//         $('#columns').waitForDisplayed();
//         const colA = $('#column-a');
//         const colB = $('#column-b');

//         assert.equal(colA.isDisplayed(), true, 'block is not displayed');
//         assert.equal(colB.isDisplayed(), true, 'block is not displayed');

//         assert.equal(colA.getText(), 'A', 'Text in block "A" is incorrect');
//         assert.equal(colB.getText(), 'B', 'Text in block "B" is incorrect');

//         //here I use HTML Drag and Drop Simulator 
//         //from here https://github.com/html-dnd/html-dnd
//         browser.execute(dragAndDrop, '#column-a', '#column-b');

//         assert.equal(colA.getText(), 'B', 'Text in block "A" is not changing');
//         assert.equal(colB.getText(), 'A', 'Text in block "B" is not changing');

//     });
// });

describe('Dropdown List', () => {
    it('Test selection from dropdown list', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Dropdown').click();
        const dropdown = $('#dropdown');
        const opt1 = $('#dropdown > option:nth-child(2)');
        const opt2 = $('#dropdown > option:nth-child(3)');
        dropdown.waitForDisplayed();
        dropdown.click();
        opt1.waitForDisplayed();
        opt1.click();
        assert.equal(opt1.getText(), 'Option 1', 'Value 1 text is incorrect');
        assert.equal(opt1.getAttribute('selected'), 'true', 'Value 1 is not selected');
        dropdown.click();
        opt2.waitForDisplayed();
        opt2.click();
        assert.equal(opt2.getText(), 'Option 2', 'Value 2 text is incorrect');
        assert.equal(opt2.getAttribute('selected'), 'true', 'Value 2 is not selected');
    });
});

describe('Dynamic Content', () => {
    it('Test dynamic refreshing content&picture', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Dynamic Content').click();
        $('#content').waitForDisplayed();
        $('#content > div:nth-child(1)').waitForDisplayed();
        const picVar1 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const textVar1 = $('#content > div:nth-child(7) > div.large-10.columns').getText();
        browser.refresh();
        $('#content').waitForDisplayed();
        let picVar2 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const textVar2 = $('#content > div:nth-child(7) > div.large-10.columns').getText();
        while (picVar1 == picVar2) {
            console.log('enter to loop 1');
            browser.refresh();
            $('#content').waitForDisplayed();
            picVar2 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        };
        assert.notEqual(picVar1, picVar2, 'Picture not changes on stage 1');
        assert.notEqual(textVar1, textVar2, 'Text not changes on stage 1');

        //changing text and picture with some static content

        browser.url('http://the-internet.herokuapp.com');
        $('a=Dynamic Content').click();
        $('#content').waitForDisplayed();
        $('.example > p:nth-child(3) > a').click();
        const picVarNew1 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const textVarNew1 = $('#content > div:nth-child(7) > div.large-10.columns').getText();
        const picVarNoChange = $('#content > div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const textVarNoChange = $('#content > div:nth-child(4) > div.large-10.columns').getText();
        browser.refresh();
        $('#content').waitForExist();
        let picVarNew2 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const textVarNew2 = $('#content > div:nth-child(7) > div.large-10.columns').getText();
        const picVarNoChange1 = $('#content > div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const textVarNoChange1 = $('#content > div:nth-child(4) > div.large-10.columns').getText();
        while (picVarNew1 == picVarNew2) {
            console.log('enter to loop 2');
            browser.refresh();
            $('#content').waitForDisplayed();
            picVarNew2 = $('#content > div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        };
        assert.notEqual(picVarNew1, picVarNew2, 'Picture not changes');
        assert.notEqual(textVarNew1, textVarNew2, 'Text not changes');
        assert.strictEqual(picVarNoChange, picVarNoChange1, 'Picture is not the same');
        assert.strictEqual(textVarNoChange, textVarNoChange1, 'Text is not the same');
    });
});

describe('Dynamic Controls', () => {
    it('Test dynamic refreshing content&picture', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Dynamic Controls').click();
        $('#content').waitForDisplayed();

        // Test remove/add checkbox

        assert.equal($('#checkbox').isDisplayed(), true, 'Checkbox is invisible');
        $('#checkbox-example > button').click();
        $('#checkbox-example > #message').waitForDisplayed();
        assert.strictEqual($('#checkbox-example > #message').getText(), 'It\'s gone!', 'Checkbox still is visible');
        $('#checkbox-example > button').click();
        $('#checkbox-example > #message').waitForDisplayed();
        assert.strictEqual($('#checkbox-example > #message').getText(), 'It\'s back!', 'Checkbox is not showing');

        // Test enable/disable field

        assert.equal($('input[type=text]').isEnabled(), false, 'Input field is enabled');
        $('#input-example > button').click();
        $('#input-example > #message').waitForDisplayed();
        assert.strictEqual($('#input-example > #message').getText(), 'It\'s enabled!', 'Input field is not enabled');
        assert.equal($('input[type=text]').isEnabled(), true, 'Input field is not enabled');
        $('#input-example > button').click();
        $('#input-example > #message').waitForExist();
        assert.strictEqual($('#input-example > #message').getText(), 'It\'s disabled!', 'Input field is enabled');
    });
});

describe('Dynamic Loading', () => {
    it('Test element on page that is hidden', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Dynamic Loading').click();
        $('#content').waitForDisplayed();
        assert.equal($('#content > div > p:nth-child(3)').getText(), 'There are two examples. One in which an element already exists on the page but it is not displayed. And anonther where the element is not on the page and gets added in.', 'Content text is incorrect');

        // Example 1: Element on page that is hidden
        $('[href="/dynamic_loading/1"]').click();
        $('#start').waitForDisplayed();
        $('#start > button').click();
        $('#finish > h4').waitForDisplayed(10000);
        assert.equal($('#finish > h4').getText(), 'Hello World!', 'Message is not visible');
    });
});

describe('Dynamic Loading', () => {
    it('Test element rendered after the fact', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Dynamic Loading').click();
        $('#content').waitForDisplayed();
        assert.equal($('#content > div > p:nth-child(3)').getText(), 'There are two examples. One in which an element already exists on the page but it is not displayed. And anonther where the element is not on the page and gets added in.', 'Content text is incorrect');

        //Example 2: Element rendered after the fact
        $('[href="/dynamic_loading/2"]').click();
        $('#start').waitForDisplayed();
        $('#start > button').click();
        $('#finish > h4').waitForDisplayed(10000);
        assert.strictEqual($('#finish > h4').getText(), 'Hello World!', 'Message is not visible');
    });
});

describe('Entry Ad', () => {
    it('Test that modal window shows when page loads', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Entry Ad').click();
        $('.modal').waitForDisplayed();
        var content = $('div.modal-body > p').getText();
        assert.strictEqual(content, "It's commonly used to encourage a user to take an action (e.g., give their e-mail address to sign up for something or disable their ad blocker).")
        $('.modal-footer > p').click();
        $('.example').waitForDisplayed();
        do {
            $('#restart-ad').click();
            browser.pause(1500);
        } while ($('.modal').isDisplayed() == false);
        assert.strictEqual(content, "It's commonly used to encourage a user to take an action (e.g., give their e-mail address to sign up for something or disable their ad blocker).")
        $('.modal-footer > p').click();
    });
});

describe('Exit Intent', () => {
    it('Mouse out of the viewport pane and see a modal window appear', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Exit Intent').click();
        $('#content > div.example > p').waitForDisplayed();
        assert.strictEqual($('#content > div.example > p').getText(), "Mouse out of the viewport pane and see a modal window appear.");
        $('#flash-messages').moveTo(50, 50);
        $('#flash-messages').moveTo(50, -50);
        $('#ouibounce-modal > div.modal').waitForDisplayed();
        var content = $('div.modal-body > p').getText();
        assert.strictEqual(content, "It's commonly used to encourage a user to take an action (e.g., give their e-mail address to sign up for something).")
        $('.modal-footer > p').click();
    });
});

describe('File Uploader', () => {
    it('Test uploading file', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=File Upload').click();
        const fileUpload = $('#file-upload');
        const filePath = path.join(__dirname, '../upload/test.txt');
        fileUpload.waitForDisplayed();
        fileUpload.setValue(filePath);
        browser.pause(3000);
        $('#file-submit').click();
        browser.pause(3000);
        $('#uploaded-files').waitForDisplayed();
        assert.strictEqual($('#uploaded-files').getText(), 'test.txt');
    });
});

describe('Floating Menu', () => {
    it('Test floating Menu', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Floating Menu').click();
        const home = $('#menu');
        $('#content > div > h3').waitForDisplayed();
        assert.equal($('#content > div > h3').getText(), 'Floating Menu');
        const firstPositionHome = home.getAttribute('style');
        browser.keys('End');
        $('a=Home').waitForDisplayed();
        const secondPositionHome = home.getAttribute('style');
        const positionCompare = (firstPositionHome === secondPositionHome) ? 'true' : 'false';
        assert.equal(positionCompare, 'false', 'menu position is not changing');
        $('a=Home').click();
        assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/floating_menu#home');
    });
});

describe('Forgot Password', () => {
    it('Test email sending', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Forgot Password').click();
        const emailField = $('#email');
        const submitButton = $('#form_submit');
        const validEmail = 'test@test.com';
        const invalidEmail = '123@com.';
        emailField.waitForDisplayed();
        assert.equal(submitButton.isDisplayed(), true, 'Button is not visible');

        //check valid input

        emailField.setValue(validEmail);
        submitButton.click();
        assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/email_sent', 'E-mail was not send');
        assert.strictEqual($('#content').getText(), 'Your e-mail\'s been sent!', 'E-mail was not send');

        //check invalid input

        browser.url('http://the-internet.herokuapp.com/forgot_password');
        emailField.waitForDisplayed();
        assert.equal(submitButton.isDisplayed(), true, 'Button is not visible');

        try {
            emailField.setValue(invalidEmail);
            submitButton.click();
            assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/email_sent', 'E-mail was not send');
            if ($('#content').getText() === 'Your e-mail\'s been sent!') {
                throw new SyntaxError("Warning! Invalid test have been PASSED");
            };
        } catch (error) {
            console.log('!!!' + error);
        };

        //check empty input

        let xhr = new XMLHttpRequest();

        browser.url('http://the-internet.herokuapp.com/forgot_password');
        emailField.waitForDisplayed();
        assert.equal(submitButton.isDisplayed(), true, 'Button is not visible');
        submitButton.click();
        xhr.open('POST', browser.getUrl(), false);
        xhr.send();
        console.log(xhr.status);
        assert.equal(xhr.readyState, '4', 'Query status is incorrect');
        assert.equal(xhr.status, '500', 'Server response failure');
        $('h1=Internal Server Error').waitForDisplayed();
        assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/forgot_password', 'Empty input was PASSED');
        assert.strictEqual($('body > h1').getText(), 'Internal Server Error', 'Empty input was PASSED');
    });
});

describe('Form Authentication', () => {
    it('Test login and password input', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Form Authentication').click();
        const validUsername = 'tomsmith';
        const invalidUsername1 = '          ';
        const invalidUsername2 = '«»‘~!@#$%^&*()?>,./\<][ /*<!—«»';
        const invalidUsername3 = 'томсмит';
        const invalidUsername4 = 'TOMSMITH';
        const invalidUsername5 = ' tomsmith ';
        const validPass = 'SuperSecretPassword!';
        const invalidPass1 = '          !';
        const invalidPass2 = '«»‘~!@#$%^&*()?>,./\<][ /*<!—«»!';
        const invalidPass3 = 'суперсекретныйпароль!';
        const invalidPass4 = 'SUPERSECRETPASSWORD!';
        const invalidPass5 = ' SuperSecretPassword! ';
        const inputUsername = $('#username');
        const inputPass = $('#password');
        const loginButton = $('.radius');
        const errorMessage = $('.flash.error');
        const successMessage = $('.flash.success');
        $('#login').waitForDisplayed();
        $('.radius').isDisplayed();

        //test valid username and password

        inputUsername.setValue(validUsername);
        inputPass.setValue(validPass);
        loginButton.click();
        $('.button.secondary.radius').waitForDisplayed();
        assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/secure');
        assert.strictEqual(successMessage.getText(), 'You logged into a secure area!\n×');
        $('.button.secondary.radius').click();
        assert.strictEqual(browser.getUrl(), 'http://the-internet.herokuapp.com/login');
        assert.strictEqual(successMessage.getText(), 'You logged out of the secure area!\n×');

        //test invalid username and valid password

        inputUsername.setValue(invalidUsername1);
        inputPass.setValue(validPass);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        //test valid username and invalid password

        inputUsername.setValue(validUsername);
        inputPass.setValue(invalidPass1);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your password is invalid!\n×');

        //test invalid username and invalid password

        inputUsername.setValue(invalidUsername1);
        inputPass.setValue(invalidPass1);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        //test input only valid username

        inputUsername.setValue(validUsername);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your password is invalid!\n×');

        //test input only valid password

        inputPass.setValue(validPass);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        //test some types of invalid username and invalid password

        inputUsername.setValue(invalidUsername2);
        inputPass.setValue(invalidPass2);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        inputUsername.setValue(invalidUsername3);
        inputPass.setValue(invalidPass3);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        inputUsername.setValue(invalidUsername4);
        inputPass.setValue(invalidPass4);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

        inputUsername.setValue(invalidUsername5);
        inputPass.setValue(invalidPass5);
        loginButton.click();
        assert.strictEqual(errorMessage.getText(), 'Your username is invalid!\n×');

    });
});

describe('Frames', () => {
    it('Test frame content', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Frames').click();

        //test link - Nested Frames

        $('a=Nested Frames').waitForDisplayed();
        $('a=Nested Frames').click();
        $('html > frameset').waitForDisplayed();
        browser.switchToFrame('frame-top');
        browser.switchToFrame('frame-left');
        assert.strictEqual($('body').getText(), 'LEFT');
        browser.switchToParentFrame();
        browser.switchToFrame('frame-middle');
        assert.strictEqual($('#content').getText(), 'MIDDLE');
        browser.switchToParentFrame();
        browser.switchToFrame('frame-right');
        assert.strictEqual($('body').getText(), 'RIGHT');
        browser.switchToParentFrame();
        browser.switchToParentFrame();
        browser.switchToFrame('frame-bottom');
        assert.strictEqual($('body').getText(), 'BOTTOM');

        //test link - iFrame

        browser.url('http://the-internet.herokuapp.com');
        $('a=Frames').click();
        $('a=iFrame').waitForDisplayed();
        $('a=iFrame').click();
        $('#mceu_13').waitForDisplayed();
        browser.switchToFrame('mce_0_ifr')
        assert.strictEqual($('#tinymce > p').getText(), 'Your content goes here.');
        $('#tinymce > p').clearValue();
        assert.strictEqual($('#tinymce > p').getText(), '');
        $('#tinymce').setValue('Hello World!');
        assert.strictEqual($('#tinymce > p').getText(), 'Hello World!');
        browser.switchToParentFrame();
        assert.strictEqual($('#content > div > h3').getText(), 'An iFrame containing the TinyMCE WYSIWYG Editor');

    });
});

//Test work only without headless mode

// describe('Geolocation', () => {
//     it('Test location info', () => {
//         browser.url('https://the-internet.herokuapp.com');
//         $('a=Geolocation').click();
//         $('#demo').waitForDisplayed();
//         assert.strictEqual($('#demo').getText(), 'Click the button to get your current latitude and longitude');
//         const button = $('.example > button');
//         assert.strictEqual(button.isDisplayed(), true, 'Button is not visible');
//         assert.strictEqual(button.getText(), 'Where am I?', 'Text on button is incorrect');
//         button.click();
//         $('#lat-value').waitForDisplayed();
//         const latitude = $('#lat-value');
//         const longitude = $('#long-value');
//         const googleLink = $('#map-link > a');
//         assert.strictEqual(latitude.isDisplayed(), true, 'Latitude is not displaing');
//         assert.strictEqual(longitude.isDisplayed(), true, 'Longitude is not displaing');
//         const latValue = Number(latitude.getText());
//         const longValue = Number(longitude.getText()) + Number(0.0000001);
//         assert.strictEqual(googleLink.isDisplayed(), true, 'Google link is not visible');
//         googleLink.click();

//         //compare latitude&longitude with values from google maps
//         $('body > jsl').waitForDisplayed();
//         const geolocation = latValue.toFixed(6) + ', ' + longValue.toFixed(6);
//         const geolocationGoogle = $('#pane > div > div.widget-pane-content.scrollable-y > div > div > div.section-hero-header-title > div.section-hero-header-title-description > h2 > span').getText();
//         assert.equal(geolocation, geolocationGoogle, 'Location is invalid');

//     });
// });

describe('Horizontal Slider', () => {
    it('Test slider to show correct value', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Horizontal Slider').click();
        $('.subheader').waitForDisplayed();
        assert.strictEqual($('.subheader').getText(), 'Set the focus on the slider (by clicking on it) and use the arrow keys to move it right and left. Or click and drag the slider with your mouse. It will indicate the value of the slider to the right.')
        assert.strictEqual($('.sliderContainer').isDisplayed(), true);
        const slider = $('#content > div > div > input[type=range]');
        assert.strictEqual($('#range').getText(), '0');
        slider.click();
        assert.strictEqual($('#range').getText(), '2.5');
        browser.keys('ArrowRight');
        assert.strictEqual($('#range').getText(), '3');
        browser.keys('ArrowLeft');
        assert.strictEqual($('#range').getText(), '2.5');
        browser.keys('End');
        assert.strictEqual($('#range').getText(), '5');
        browser.keys('Home');
        assert.strictEqual($('#range').getText(), '0');

    });
});

describe('Hovers', () => {
    it('Test showing info under user photo', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Hovers').click();
        $('#content > div > p').waitForDisplayed();
        assert.strictEqual($('#content > div > p').getText(), 'Hover over the image for additional information');
        $('#content > div > div:nth-child(3) > img').moveTo();
        assert.equal($('#content > div > div:nth-child(3) > div > a').isDisplayed(), true);
        assert.equal($('#content > div > div:nth-child(3) > div > h5').isDisplayed(), true);
        assert.strictEqual($('#content > div > div:nth-child(3) > div > h5').getText(), 'name: user1');
        assert.strictEqual($('#content > div > div:nth-child(3) > div > a').getText(), 'View profile');
        $("a[href='/users/1']").click();
        $('body > h1').waitForDisplayed();
        assert.strictEqual($('body > h1').getText(), 'Not Found');
        browser.url('http://the-internet.herokuapp.com/hovers');
        $('#content > div > div:nth-child(4) > img').moveTo();
        assert.equal($('#content > div > div:nth-child(4) > div > a').isDisplayed(), true);
        assert.equal($('#content > div > div:nth-child(4) > div > h5').isDisplayed(), true);
        assert.strictEqual($('#content > div > div:nth-child(4) > div > h5').getText(), 'name: user2');
        assert.strictEqual($('#content > div > div:nth-child(4) > div > a').getText(), 'View profile');
        $("a[href='/users/2']").click();
        $('body > h1').waitForDisplayed();
        assert.strictEqual($('body > h1').getText(), 'Not Found');
        browser.url('http://the-internet.herokuapp.com/hovers');
        $('#content > div > div:nth-child(5) > img').moveTo();
        assert.equal($('#content > div > div:nth-child(5) > div > a').isDisplayed(), true);
        assert.equal($('#content > div > div:nth-child(5) > div > h5').isDisplayed(), true);
        assert.strictEqual($('#content > div > div:nth-child(5) > div > h5').getText(), 'name: user3');
        assert.strictEqual($('#content > div > div:nth-child(5) > div > a').getText(), 'View profile');
        $("a[href='/users/3']").click();
        $('body > h1').waitForDisplayed();
        assert.strictEqual($('body > h1').getText(), 'Not Found');
        browser.url('http://the-internet.herokuapp.com/hovers');

    });
});

describe('Infinite Scroll', () => {
    it('Test infinite scroll', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Infinite Scroll').click();
        assert.equal($('#content >  div > div > div > div > div:nth-child(1)').waitForDisplayed(), true, "page is not loading");

        const sum = [];

        function Loop() {
            for (let i = 2; i < 10; i++) {
                browser.keys('End');
                browser.pause(1000);
                ($('#content > div > div > div > div > div:nth-child(' + [i] + ')').waitForDisplayed()) ? sum.push([i]) : "new paragraph does not load in loop";
            };
            return sum.length;
        };
        assert.equal($('#content > div > div > div > div > div:nth-child(' + Loop() + ')').isDisplayed(), true, "new paragraph does not load");

        //version 2 (simple)

        // browser.keys('PageDown');
        // assert.equal($('#content > div > div > div > div > div:nth-child(3)').waitForDisplayed(), true, "new paragraph does not load");
        // browser.keys('PageDown');
        // assert.equal($('#content > div > div > div > div > div:nth-child(4)').waitForDisplayed(), true, "new paragraph does not load");
        // browser.keys('PageDown');
        // assert.equal($('#content > div > div > div > div > div:nth-child(5)').waitForDisplayed(), true, "new paragraph does not load");
        // browser.refresh();
        // assert.equal($('#content > div > div > div > div > div:nth-child(3)').isExisting(), false, "only 2 paragraphs can load on first load");

    });
});

describe('Inputs', () => {
    it('Test input field', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Inputs').click();
        const inputField = $('#content > div > div > div > input[type=number]');
        var maxCountUp = 0;
        var maxCountDown = 0;
        inputField.waitForDisplayed();
        inputField.click();
        assert.equal(inputField.isFocused(), true, 'input field is not focused');

        //checking the display of values by pressing Arrow Up & Arrow Down

        function LoopInputUp() {
            for (i = 1; i <= 10; i++) {
                browser.keys('ArrowUp');
                maxCountUp = +i;
            }
            return maxCountUp;
        };
        console.log(LoopInputUp());
        assert.equal(inputField.getValue(), LoopInputUp(), 'something goes wrong with LoopInputUp');

        inputField.clearValue();
        inputField.click();

        function LoopInputDown() {
            for (i = -1; i >= -10; i--) {
                browser.keys('ArrowDown');
                maxCountDown = +i;
            }
            return maxCountDown;
        };

        console.log(LoopInputDown());
        assert.equal(inputField.getValue(), LoopInputDown(), 'something goes wrong with LoopInputDown');

        //checking the direct transfer of value to the input field

        inputField.setValue('01234');
        browser.keys('ArrowUp');
        assert.equal(inputField.getValue(), '1235', 'input field error when set value to 1235');
        inputField.setValue('input words');
        assert.notEqual(inputField.getValue(), 'input words', 'input field accept input words');
        inputField.setValue('!@#$%^&*()');
        assert.notEqual(inputField.getValue(), '!@#$%^&*()', 'input field accept special symbols');
        inputField.setValue('999999999999999999');
        browser.keys('ArrowUp');
        assert.equal(inputField.getValue(), '1e+18', 'input field accept more than 18 symbols');
        inputField.setValue('-999999999999999999');
        browser.keys('ArrowDown');
        assert.equal(inputField.getValue(), '-1e+18', 'input field accept more than 18 symbols');
        inputField.setValue(Math.random());
        browser.keys('ArrowDown');
        console.log(inputField.getValue());
        assert.equal(inputField.getValue(), '0', 'does not round input value');
        inputField.setValue('-1,6'.replace(',', '.'));
        browser.keys('ArrowDown');
        console.log(inputField.getValue());
        assert.equal(inputField.getValue(), '-2', 'does not round input value');

    });
});

describe('JQuery UI Menus', () => {
    it('Test JQuery menu funcionality', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JQuery UI Menus').click();
        $('#menu').waitForDisplayed();

        //testing menu UI

        $('a=Enabled').click();
        $('a=Downloads').waitForDisplayed();
        assert.equal($('a=Back to JQuery UI').isDisplayed(), true, 'menu option is not visible');
        $('a=Downloads').click();
        $('a=PDF').waitForDisplayed();
        assert.equal($('a=CSV').isDisplayed(), true, 'menu option is not visible');
        assert.equal($('a=Excel').isDisplayed(), true, 'menu option is not visible');

        //testing menu functions

        $('a=Back to JQuery UI').click();
        $('[href="/jqueryui/menu"]').waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/jqueryui', 'url adress is incorrect');
        $('[href="/jqueryui/menu"]').click();
        $('#menu').waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/jqueryui/menu', 'home page url is incorrect');

        // case to download straight

        // var pathPDF = $('a=PDF').getAttribute('href');
        // var pathCSV = $('a=CSV').getAttribute('href');
        // var pathExcel = $('a=Excel').getAttribute('href');
        // browser.url(pathPDF);
        // browser.url(pathCSV);
        // browser.url(pathExcel);

    });
});

describe('JQuery UI Menus', () => {
    it('Test JQuery PDF download', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JQuery UI Menus').click();
        $('#menu').waitForDisplayed();

        //PDF download

        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://the-internet.herokuapp.com/download/jqueryui/menu/menu.pdf', false);
        xhr.send();
        assert.equal(xhr.readyState, '4', 'Query status is incorrect');
        assert.equal(xhr.status, '200', 'Server response failure');
        assert.notEqual(xhr.getResponseHeader('Content-Length'), '0', 'File size is 0 kb');

        //case without headless

        // const menuEnabled = $('#menu>li:nth-child(2)');
        // const menuDownload = $('#menu>li:nth-child(2)>ul>li');
        // const menuPdfDownload = $('#menu>li:nth-child(2)>ul>li>ul>li:nth-child(1)>a');

        // browser.execute('arguments[0].click()', menuEnabled);
        // browser.execute('arguments[0].click()', menuDownload);
        // browser.execute('arguments[0].click()', menuPdfDownload);

        // const downloadUrl = menuPdfDownload.getAttribute('href');
        // const splitPath = downloadUrl.split('/');
        // const fileName = splitPath.splice(-1)[0];
        // const filePath = path.join(global.downloadDir, fileName);
        // browser.call(function () {
        //     return waitForFileExists(filePath, 60000);
        // });

    });
});

describe('JQuery UI Menus', () => {
    it('Test JQuery CSV download', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JQuery UI Menus').click();
        $('#menu').waitForDisplayed();

        //CSV download

        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://the-internet.herokuapp.com/download/jqueryui/menu/menu.csv', false);
        xhr.send();
        assert.equal(xhr.readyState, '4', 'Query status is incorrect');
        assert.equal(xhr.status, '200', 'Server response failure');
        assert.notEqual(xhr.getResponseHeader('Content-Length'), '0', 'File size is 0 kb');

        //case without headless

        // const menuEnabled = $('#menu>li:nth-child(2)');
        // const menuDownload = $('#menu>li:nth-child(2)>ul>li');
        // const menuCsvDownload = $('#menu>li:nth-child(2)>ul>li>ul>li:nth-child(2)>a');

        // browser.execute('arguments[0].click()', menuEnabled);
        // browser.execute('arguments[0].click()', menuDownload);
        // browser.execute('arguments[0].click()', menuCsvDownload);

        // const downloadUrl = menuCsvDownload.getAttribute('href');
        // const splitPath = downloadUrl.split('/');
        // const fileName = splitPath.splice(-1)[0];
        // const filePath = path.join(global.downloadDir, fileName);
        // browser.call(function () {
        //     return waitForFileExists(filePath, 60000);
        // });

    });
});

describe('JQuery UI Menus', () => {
    it('Test JQuery Excel download', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JQuery UI Menus').click();
        $('#menu').waitForDisplayed();

        //Excel download

        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://the-internet.herokuapp.com/download/jqueryui/menu/menu.xls', false);
        xhr.send();
        assert.equal(xhr.readyState, '4', 'Query status is incorrect');
        assert.equal(xhr.status, '200', 'Server response failure');
        assert.notEqual(xhr.getResponseHeader('Content-Length'), '0', 'File size is 0 kb');

        //case without headless

        // const menuEnabled = $('#menu>li:nth-child(2)');
        // const menuDownload = $('#menu>li:nth-child(2)>ul>li');
        // const menuExcelDownload = $('#menu>li:nth-child(2)>ul>li>ul>li:nth-child(3)>a');

        // browser.execute('arguments[0].click()', menuEnabled);
        // browser.execute('arguments[0].click()', menuDownload);
        // browser.execute('arguments[0].click()', menuExcelDownload);

        // const downloadUrl = menuExcelDownload.getAttribute('href');
        // const splitPath = downloadUrl.split('/');
        // const fileName = splitPath.splice(-1)[0];
        // const filePath = path.join(global.downloadDir, fileName);
        // browser.call(function () {
        //     return waitForFileExists(filePath, 60000);
        // });

    });
});

describe('JavaScript Alerts', () => {
    it('Test alert actions, JS promt input', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JavaScript Alerts').click();
        $('#content > div > ul').waitForDisplayed();

        //jsAlert button test

        const jsAlertBtn = $('#content > div > ul > li:nth-child(1) > button');
        assert.equal(jsAlertBtn.getText(), 'Click for JS Alert', 'Incorrect jsAlert button name');
        jsAlertBtn.click();
        assert.equal(browser.getAlertText(), 'I am a JS Alert', 'Incorrect JS alert text');
        browser.acceptAlert();
        const resultTextAlert = $('#result').getAttribute("innerText");
        assert.equal(resultTextAlert, 'You successfuly clicked an alert', 'Incorrect jsAlert result text');

        //jsConfirm button test

        const jsConfirmBtn = $('#content > div > ul > li:nth-child(2) > button');
        assert.equal(jsConfirmBtn.getText(), 'Click for JS Confirm', 'Incorrect jsConfirm button name');
        jsConfirmBtn.click();
        assert.equal(browser.getAlertText(), 'I am a JS Confirm', 'Incorrect JS confirm text');
        browser.acceptAlert();
        const resultTextConfirm = $('#result').getAttribute("innerText");
        assert.equal(resultTextConfirm, 'You clicked: Ok', 'Incorrect jsConfirm result text');
        jsConfirmBtn.click();
        browser.dismissAlert();
        const resultTextDismiss = $('#result').getAttribute("innerText");
        assert.equal(resultTextDismiss, 'You clicked: Cancel', 'Incorrect jsDismiss result text');

        //jsPrompt button test

        const jsPromptBtn = $('#content > div > ul > li:nth-child(3) > button');
        assert.equal(jsPromptBtn.getText(), 'Click for JS Prompt', 'Incorrect jsPromt button name');
        jsPromptBtn.click();
        assert.equal(browser.getAlertText(), 'I am a JS prompt', 'Incorrect JS promt text');
        browser.acceptAlert();
        const resultTextPromtConfirmEmpty = $('#result').getAttribute("innerText");
        assert.equal(resultTextPromtConfirmEmpty, 'You entered:', 'Incorrect JS promt result: confirm empty');
        jsPromptBtn.click();
        browser.dismissAlert();
        const resultTextPromtDismiss = $('#result').getAttribute("innerText");
        assert.equal(resultTextPromtDismiss, 'You entered: null', 'Incorrect JS promt result: dismiss');
        jsPromptBtn.click();
        browser.sendAlertText('input some text');
        browser.acceptAlert();
        const resultTextPromtConfirmValue1 = $('#result').getAttribute("innerText");
        assert.equal(resultTextPromtConfirmValue1, 'You entered: input some text', 'Incorrect JS promt result: input some text');
        jsPromptBtn.click();
        browser.sendAlertText('0123456789');
        browser.acceptAlert();
        const resultTextPromtConfirmValue2 = $('#result').getAttribute("innerText");
        assert.equal(resultTextPromtConfirmValue2, 'You entered: 0123456789', 'Incorrect JS promt result: input numbers');
        jsPromptBtn.click();
        browser.sendAlertText('~!@#$%^&*()');
        browser.acceptAlert();
        const resultTextPromtConfirmValue3 = $('#result').getAttribute("innerText");
        assert.equal(resultTextPromtConfirmValue3, 'You entered: ~!@#$%^&*()', 'Incorrect JS promt result: input some symbols');

        browser.refresh();
        assert.equal($('#result').getAttribute("innerText"), '', 'Incorrect result after page refresh');

    });
});

describe('JavaScript onload event error', () => {
    it('Test for errors onload', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=JavaScript onload event error').click();
        $('body > p').waitForDisplayed();
        assert.equal($('body > p').getText(), 'This page has a JavaScript error in the onload event. This is often a problem to using normal Javascript injection techniques.');
        const loadErr = $('body').getAttribute('onload');

        try {
            browser.execute(loadErr);
        } catch (err) {
            const messageLength = err.message.indexOf('  (');
            const newErrMessage = err.message.slice(0, messageLength);
            assert.equal(newErrMessage, 'unknown error: Cannot read property \'xyz\' of undefined\n', 'There is another error in function');
        };

    });
});

describe('Key Presses', () => {
    it('Test keys input form', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Key Presses').click();
        $('#content > div > form').waitForDisplayed();
        var timestamp = date.toString().slice(0, date.toString().indexOf(' GMT'));
        timestamp = timestamp.toString().replace(/ |:/g, '_');
        const inputField = $('#target');
        const result = $('#result');
        const alphabet = 'abcdefghijklmnopqrstuvwxyz';
        const numbers = '0123456789';
        const symbols = '!@#$%^&*()';
        const buttons = 'Tab,Shift,Control,Alt,Escape,End,Home,Insert,Delete';
        const buttonsArr = buttons.split(',');

        for (let i = 0; i < alphabet.length; i++) {
            inputField.setValue(alphabet[i]);
            assert.equal(result.getText(), 'You entered: ' + alphabet[i].toUpperCase(), 'Error in result field (alphabet)');
            assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
            inputField.clearValue();
        };

        for (let i = 0; i < numbers.length; i++) {
            inputField.setValue(numbers[i]);
            assert.equal(result.getText(), 'You entered: ' + numbers[i], 'Error in result field (numbers)');
            assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
            inputField.clearValue();
        };

        //Test failing symbols (screenshot will be saved in reports folder)

        // for (let i = 0; i < symbols.length; i++) {
        //     inputField.setValue(symbols[i]);
        //     assert.equal(result.getText(), 'You entered: ' + symbols[i], 'Error in result field (symbols)');
        //     assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
        //     inputField.clearValue();
        // };

        //test for continuous testing symbols

        // try {
        //     for (let i = 0; i < symbols.length; i++) {
        //         inputField.clearValue();
        //         inputField.setValue(symbols[i]);
        //         assert.equal(result.getText(), 'You entered: ' + symbols[i], 'Error in result field (symbols)');
        //         assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
        //     };
        // } catch (err) {
        //     const error = err.name.slice(0, err.name.indexOf(' ['));
        //     const filePath = path.join(__dirname, '../screenshots/' + error + '_' + timestamp + '.png');
        //     browser.saveScreenshot(filePath);
        // };

        //Test failing buttons (screenshot will be saved in reports folder)

        // for (let i = 0; i < buttons.length; i++) {
        //     inputField.setValue(buttons[i]);
        //     assert.equal(result.getText(), 'You entered: ' + buttons[i].toUpperCase(), 'Error in result field (buttons)');
        //     assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
        //     inputField.clearValue();
        // };

        for (let i = 0; i < buttonsArr.length; i++) {
            inputField.click();
            assert.equal(inputField.isFocused(), true, 'Input field is not in focused (buttons)');
            browser.keys(buttonsArr[i]);
            assert.equal(result.getText(), 'You entered: ' + buttonsArr[i].toUpperCase(), 'Error in result field (buttons)');
            assert.equal(result.getAttribute('style'), 'color: green;', 'Text color is not green');
        };

    });
});