const assert = require('assert');
const path = require('path');
const date = new Date;
// const defaults = require('D://webdriverio-test/wdio.conf').config;
// const waitForFileExists = require('../../test/util/waitForFileExists');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const dragAndDrop = require('html-dnd').codeForSelectors;

describe('Large & Deep DOM', () => {
    it('Test deep DOM selector', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Large & Deep DOM').click();
        $('#siblings').waitForDisplayed();
        arr = '1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 3.1, 3.2, 3.3, 4.1, 4.2, 4.3, 5.1, 5.2, 5.3, 6.1, 6.2, 6.3, 7.1, 7.2, 7.3, 8.1, 8.2, 8.3, 9.1, 9.2, 9.3, 10.1, 10.2, 10.3, 11.1, 11.2, 11.3, 12.1, 12.2, 12.3, 13.1, 13.2, 13.3, 14.1, 14.2, 14.3, 15.1, 15.2, 15.3, 16.1, 16.2, 16.3, 17.1, 17.2, 17.3, 18.1, 18.2, 18.3, 19.1, 19.2, 19.3, 20.1, 20.2, 20.3, 21.1, 21.2, 21.3, 22.1, 22.2, 22.3, 23.1, 23.2, 23.3, 24.1, 24.2, 24.3, 25.1, 25.2, 25.3, 26.1, 26.2, 26.3, 27.1, 27.2, 27.3, 28.1, 28.2, 28.3, 29.1, 29.2, 29.3, 30.1, 30.2, 30.3, 31.1, 31.2, 31.3, 32.1, 32.2, 32.3, 33.1, 33.2, 33.3, 34.1, 34.2, 34.3, 35.1, 35.2, 35.3, 36.1, 36.2, 36.3, 37.1, 37.2, 37.3, 38.1, 38.2, 38.3, 39.1, 39.2, 39.3, 40.1, 40.2, 40.3, 41.1, 41.2, 41.3, 42.1, 42.2, 42.3, 43.1, 43.2, 43.3, 44.1, 44.2, 44.3, 45.1, 45.2, 45.3, 46.1, 46.2, 46.3, 47.1, 47.2, 47.3, 48.1, 48.2, 48.3, 49.1, 49.2, 49.3, 50.1, 50.2, 50.3';
        const arrSplit = arr.split(', ');
        const elem = $('//*[@id="sibling-1.1"]').getText();
        const elemSplit = elem.split('\n');

        for (let i = 0; i < arrSplit.length; i++) {
            assert.equal(elemSplit[i], arrSplit[i], 'Siblings value do not match');
        };

        for (let i = 1; i <= 50; i++) {
            for (let j = 1; j <= 50; j++) {
                assert.equal($('#large-table > tbody > tr.row-' + i + ' > td.column-' + j).getText(), i + '.' + j, 'table value do not match');
            };
        };

    });
});

describe('Multiple Windows', () => {
    it('Test opening link in new window', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Multiple Windows').click();
        const link = $('[href="/windows/new"]');
        link.waitForDisplayed();
        link.click();
        windowList = browser.getWindowHandles();

        for (let i = 0; i < windowList.length; i++) {
            browser.switchToWindow(windowList[i]);
            let url = browser.getUrl();
            switch (url) {
                case 'https://the-internet.herokuapp.com/windows':
                    assert.equal($('h3=Opening a new window').getText(), 'Opening a new window', 'Base url is closed');
                case 'https://the-internet.herokuapp.com/windows/new':
                    assert.equal($('h3=New Window').getText(), 'New Window', 'New window did not opened');
            };

            //another way to test that functionality

            // if (browser.getUrl() == 'https://the-internet.herokuapp.com/windows/new') {
            //     assert.equal($('h3=New Window').getText(), 'New Window', 'New window did not opened');
            // } else if (browser.getUrl() == 'https://the-internet.herokuapp.com/windows') {
            //     assert.equal($('h3=Opening a new window').getText(), 'Opening a new window', 'Base url is closed');
            // };
        };

    });
});

describe('Nested Frames', () => {
    it('Test frame content', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Nested Frames').click();
        $('html > frameset').c
        browser.switchToFrame('frame-top');
        browser.switchToFrame('frame-left');
        assert.strictEqual($('body').getText(), 'LEFT');
        browser.switchToParentFrame();
        browser.switchToFrame('frame-middle');
        assert.strictEqual($('#content').getText(), 'MIDDLE');
        browser.switchToParentFrame();
        browser.switchToFrame('frame-right');
        assert.strictEqual($('body').getText(), 'RIGHT');
        browser.switchToParentFrame();
        browser.switchToParentFrame();
        browser.switchToFrame('frame-bottom');
        assert.strictEqual($('body').getText(), 'BOTTOM');

    });
});

describe('Notification Messages', () => {
    it('Test notification message is displayed', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Notification Messages').click();

        const notification = $('.flash.notice');
        const notificationRestart = $('#content > div > p > a');
        const closeButton = $('[href="#"]');
        const content = $('#content > div > p');

        notification.waitForDisplayed();
        notificationRestart.waitForDisplayed();
        console.log(notificationRestart.waitForDisplayed());
        console.log(notification.waitForDisplayed());

        assert.notEqual(content.getText().indexOf('It is often used to convey information about an action previously taken by the user'), '-1', 'Content text is incorrect');

        closeButton.moveTo();
        closeButton.positionClick(0);
        browser.pause(1000);
        assert.equal(notification.isDisplayed(), false, 'Notification is still displayed');

        notificationRestart.click();
        notification.waitForDisplayed();
        console.log(notification.waitForDisplayed());
        console.log(notificationRestart.waitForDisplayed());

        const notificationText = notification.getText();

        switch (notificationText) {
            case 'Action successful\n×':
                console.log('case 1');
                break;

            case 'Action unsuccesful, please try again\n×':
                console.log('case 2');
                break;

            default:
                assert.equal(notification.waitForDisplayed(), true, 'Notification is not displayed');
                break;
        };

    });
});

describe('Redirect Link', () => {
    it('Test server status answer', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Redirect Link').click();
        const link = $('[href="redirect"]');
        const linkUrl = link.getAttribute('href');
        const content = $('#content > div > p');
        assert.equal(link.waitForDisplayed(), true, 'Redirection link is unavailable');
        assert.notEqual(content.getText().indexOf('Click here to trigger a redirect (and be taken to the status codes page).'), '-1', 'Content text is incorrect');

        //check server answer first redirection link from start page
        let xhr = new XMLHttpRequest();
        xhr.open('GET', linkUrl, false);
        xhr.send();
        assert.equal(xhr.readyState, '4', 'Query status is incorrect');
        assert.equal(xhr.status, '302', 'Server response failure (redirect)');

        //check redirection page
        link.click();
        const contentRedirection = $('#content > div > p');
        assert.notEqual(contentRedirection.getText().indexOf('For a complete list of status codes, go here.'), '-1', 'Content text on redirection page is incorrect');
        assert.equal($('#content > div > ul').waitForDisplayed(), true, 'Table with status codes did not displyed');
        const linkStatusArr = $('#content > div > ul').getText().split('\n');
        const arr = '200,301,404,500';
        const linkList = arr.split(',');

        for (let i = 0; i < 4; i++) {
            //check server status list
            assert.equal(linkStatusArr[i], linkList[i], 'Status list text is incorrect');

            //check server status answer each link
            xhr.open('GET', $('#content > div > ul > li:nth-child(' + (i + 1) + ') > a').getAttribute('href'), false);
            xhr.send();
            assert.equal(xhr.readyState, '4', 'Query status is incorrect');
            assert.equal(xhr.status, linkStatusArr[i], 'This page do not return a ' + linkStatusArr[i] + ' status code.');

            //check content text and go back to parent page
            $('[href="status_codes/' + linkStatusArr[i] + '"]').click();
            assert.notEqual($('#content > div > p').getText().indexOf('This page returned a ' + linkStatusArr[i] + ' status code.'), '-1', 'Content text on "Status Codes" page is incorrect');
            $('[href="/status_codes"]').waitForDisplayed();
            $('[href="/status_codes"]').click();
        };

    });
});

describe('Secure File Download', () => {
    it('Test opportunity to download file if you are logged', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Secure File Download').click();

        //if authentication window will be canceled
        browser.keys("Escape");
        $('body').waitForDisplayed();
        assert.equal($('body').getText(), 'Not authorized', 'Login and password was accepted');

        //test correct input of login and password
        browser.url('http://admin:admin@the-internet.herokuapp.com/download_secure');
        $('#content > div > h3').waitForDisplayed();
        assert.equal($('#content > div > h3').getText(), 'Secure File Downloader', 'You\'re on wrong page');

        let xhr = new XMLHttpRequest();
        fileList = $('#content > div').getText().split('\n');

        for (let i = 1; i < fileList.length; i++) {

            //check server status answer each link and file size
            xhr.open('GET', $('#content > div > a:nth-child(' + i * 2 + ')').getAttribute('href'), false, 'admin', 'admin');
            xhr.send();
            assert.equal(xhr.readyState, '4', 'Query status is incorrect');
            assert.equal(xhr.status, '200', 'This page do not return a 200 status code.');

            //extra assertion to file size
            //assert.notEqual(xhr.getResponseHeader('Content-Length'), '0', 'File size is 0 kb');
        };

    });
});

describe('Shifting Content', () => {
    it('Test different types of content shifting - Menu Element', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Shifting Content').click();
        let content = $('#content > div > p');
        content.waitForDisplayed()
        contentMain = content.getText();
        assert.equal(contentMain, 'These examples demonstrate elements on a page shifting a few pixels in either direction on each page load. This is meant to demonstrate some of the subtle rendering issues that occur across browsers.', 'The text on main page is not visible');

        //Shifting Content: Menu Element
        let btnText = 'Home, About, Contact Us, Portfolio, Gallery';
        btnText = btnText.split(', ');
        const link = $('#content > div > a:nth-child(3)');
        link.click();
        const table = $('#content > div > ul');
        const tableCell = $('#content > div > ul').getText().split('\n');
        assert.equal(table.isDisplayed(), true, 'Table is not visible');
        const loadContentRandomly = $('#content > div > p:nth-child(3) > a');
        const loadContentSpecify = $('#content > div > p:nth-child(4) > a');
        const loadContentBothMethods = $('#content > div > p:nth-child(5) > a');

        loadContentRandomly.click();
        content.waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/shifting_content/menu?mode=random', 'URL in random mode is incorrect')
        for (let i = 1; i <= tableCell.length; i++) {
            assert.equal(table.waitForDisplayed(), true, 'Table is not visible');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').isDisplayed(), true, 'Table cells are not visible when loadin randomly');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').getText(), btnText[i - 1], 'Table cells name is incorrect');
        };

        loadContentSpecify.click();
        content.waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/shifting_content/menu?pixel_shift=100', 'URL in specify mode is incorrect')
        for (let i = 1; i <= tableCell.length; i++) {
            assert.equal(table.waitForDisplayed(), true, 'Table is not visible');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').isDisplayed(), true, 'Table cells are not visible when loading specify');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').getText(), btnText[i - 1], 'Table cells name is incorrect');
        };

        loadContentBothMethods.click();
        content.waitForDisplayed();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/shifting_content/menu?mode=random&pixel_shift=100', 'URL in both methods mode is incorrect')
        for (let i = 1; i <= tableCell.length; i++) {
            assert.equal(table.waitForDisplayed(), true, 'Table is not visible');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').isDisplayed(), true, 'Table cells are not visible when loading both methods');
            assert.equal($('#content > div > ul > li:nth-child(' + i + ') > a').getText(), btnText[i - 1], 'Table cells name is incorrect');
        };

    });
});

describe('Shifting Content', () => {
    it('Test different types of content shifting - An image', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Shifting Content').click();
        let content = $('#content > div > p');
        content.waitForDisplayed()
        contentMain = content.getText();
        assert.equal(contentMain, 'These examples demonstrate elements on a page shifting a few pixels in either direction on each page load. This is meant to demonstrate some of the subtle rendering issues that occur across browsers.', 'The text on main page is not visible');

        //Shifting Content: An image
        const xhr = new XMLHttpRequest();
        const arrayUrl = '?mode=random, ?pixel_shift=100, ?mode=random&pixel_shift=100, ?image_type=simple'.split(', ');
        const link = $('#content > div > a:nth-child(6)');
        link.click();
        assert.equal($('#content > div > p:nth-child(2)').getText(), 'This example demonstrates an image shifting a few pixels in either direction on each page load.', 'Text on page is incorrect');
        const defaultUrl = browser.getUrl();
        const mainPic = $('#content > div > img');

        for (let i = 3; i <= 6; i++) {
            assert.equal($('#content > div > p:nth-child(2)').getText(), 'This example demonstrates an image shifting a few pixels in either direction on each page load.', 'Text on page is incorrect');
            const changingLink = $('#content > div > p:nth-child(' + i + ') > a').click();
            assert.equal(browser.getUrl(), defaultUrl + arrayUrl[i - 3], 'Link URL is incorrect');
            assert.equal(mainPic.isDisplayed(), true, 'Picture is not visible');
            xhr.open('GET', mainPic.getAttribute('src'), false);
            xhr.send();
            assert.equal(xhr.readyState, '4', 'Query status is incorrect');
            assert.equal(xhr.status, '200', 'This page do not return a 200 status code.');
            assert.notEqual(xhr.getResponseHeader('Content-Length'), '0', 'File size is 0 kb');
        };

    });
});

describe('Shifting Content', () => {
    it('Test different types of content shifting - An image', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Shifting Content').click();
        let content = $('#content > div > p');
        content.waitForDisplayed()
        contentMain = content.getText();
        assert.equal(contentMain, 'These examples demonstrate elements on a page shifting a few pixels in either direction on each page load. This is meant to demonstrate some of the subtle rendering issues that occur across browsers.', 'The text on main page is not visible');

        //Shifting Content: List
        const link = $('#content > div > a:nth-child(9)');
        link.click();
        $('.example').waitForDisplayed();
        assert.equal($('#content > div > p').getText(), 'This example demonstrates a list of dynamic content with a static record that constantly moves around.', 'Text on page is incorrect');
        const contentText = $('#content > div > div > div');
        assert.notEqual(contentText.getText().indexOf('Important Information You\'re Looking For'), '-1', 'Content text is incorrect');

    });
});

describe('Slow Resources', () => {
    it('Test content slow loading', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Slow Resources').click();
        const content = $('#content > div > p');
        content.waitForDisplayed();
        assert.equal(content.getText(), 'At times it can take a while for third-party site resources to load (e.g., tracking code javascript, social networking widgets, etc.). This example has a rogue GET request that takes 30 seconds to complete.', 'Content text is incorrect');

    });
});

describe('Sortable Data Tables', () => {
    it('Test content slow loading', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Sortable Data Tables').click();
        $('#table1').waitForDisplayed();

        const arrTableHead = 'Last, Name, First, Name, Email, Due, Web, Site, Action'.split(', ');
        const firstRow = 'Smith, John, jsmith@gmail.com, $50.00, http://www.jsmith.com, edit, delete'.split(', ');
        const secondRow = 'Bach, Frank, fbach@yahoo.com, $51.00, http://www.frank.com, edit, delete'.split(', ');
        const thirdRow = 'Doe, Jason, jdoe@hotmail.com, $100.00, http://www.jdoe.com, edit, delete'.split(', ');
        const fourthRow = 'Conway, Tim, tconway@earthlink.net, $50.00, http://www.timconway.com, edit, delete'.split(', ');
        const arrAll = [firstRow, secondRow, thirdRow, fourthRow];
        const tableHead = $('#table1 > thead > tr').getText().split(' ');
        const tableBody = $('#table1 > tbody > tr').getText().replace('\n', '');
        const arrTableBody = tableBody.split(' ');
        const btnLinkEdit = $('[href*="edit"]');
        const btnLinkDelete = $('[href*="delete"]');

        // Example 1 - No Class or ID attributes to signify groupings of rows and columns

        //Check table head values

        for (let i = 0; i < tableHead.length; i++) {
            assert.equal(arrTableHead[i], tableHead[i], 'Table head name is incorrect');
        };

        //check each row value

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ')').getText().split(' ');
            for (let j = 0; j < arrTableBody.length; j++) {
                assert.equal(changingRow[j], arrAll[i - 1][j], 'Value in table row #' + i + ' is incorrect');
            };
        };

        //check links

        btnLinkEdit.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/tables#edit', 'Browser URL after button click is wrong');
        btnLinkDelete.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/tables#delete', 'Browser URL after button click is wrong');

        // check table values after sort
        const arrSortLastName = [];
        const arrSortFirstName = [];
        const arrSortEmailName = [];
        const arrSortDue = [];
        const arrSortWebSite = [];
        let arrNewSort = [];

        //get values from column "Last name" and sort them
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(1)').getText();
            arrSortLastName.push(changingRow);
            arrSortLastName.sort();
        };

        //check sorting function in column "Last name"

        $('#table1 > thead > tr > th:nth-child(1)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(1)').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortLastName[i - 1], arrNewSort[i - 1], 'Sorting in column "Last name" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "First name" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(2)').getText();
            arrSortFirstName.push(changingRow);
            arrSortFirstName.sort();
        };

        //check sorting function in column "First name"

        $('#table1 > thead > tr > th:nth-child(2)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(2)').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortFirstName[i - 1], arrNewSort[i - 1], 'Sorting in column "First name" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Email" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(3)').getText();
            arrSortEmailName.push(changingRow);
            arrSortEmailName.sort();
        };

        //check sorting function in column "Email"

        $('#table1 > thead > tr > th:nth-child(3)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(3)').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortEmailName[i - 1], arrNewSort[i - 1], 'Sorting in column "Email" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Due" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(4)').getText().replace('$', '');
            arrSortDue.push(changingRow);
            arrSortDue.sort((a, b) => a - b);
        };

        //check sorting function in column "Due"

        $('#table1 > thead > tr > th:nth-child(4)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(4)').getText().replace('$', '');
            arrNewSort.push(changingRow);
            assert.equal(arrSortDue[i - 1], arrNewSort[i - 1], 'Sorting in column "Due" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Web Site" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(5)').getText();
            arrSortWebSite.push(changingRow);
            arrSortWebSite.sort();
        };

        //check sorting function in column "Web Site"

        $('#table1 > thead > tr > th:nth-child(5)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table1 > tbody > tr:nth-child(' + i + ') > td:nth-child(5)').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortWebSite[i - 1], arrNewSort[i - 1], 'Sorting in column "Web Site" is not working');
        };

    });
});

describe('Sortable Data Tables', () => {
    it('Test content slow loading', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Sortable Data Tables').click();
        $('#table2').waitForDisplayed();

        // Example 2 - Class and ID attributes to signify groupings of rows and columns

        const arrTableHead = 'Last, Name, First, Name, Email, Due, Web, Site, Action'.split(', ');
        const firstRow = 'Smith, John, jsmith@gmail.com, $50.00, http://www.jsmith.com, edit, delete'.split(', ');
        const secondRow = 'Bach, Frank, fbach@yahoo.com, $51.00, http://www.frank.com, edit, delete'.split(', ');
        const thirdRow = 'Doe, Jason, jdoe@hotmail.com, $100.00, http://www.jdoe.com, edit, delete'.split(', ');
        const fourthRow = 'Conway, Tim, tconway@earthlink.net, $50.00, http://www.timconway.com, edit, delete'.split(', ');
        const arrAll = [firstRow, secondRow, thirdRow, fourthRow];
        const tableHead = $('#table2 > thead > tr').getText().split(' ');
        const tableBody = $('#table2 > tbody > tr').getText().replace('\n', '');
        const arrTableBody = tableBody.split(' ');
        const btnLinkEdit = $('.action > a:nth-child(1)');
        const btnLinkDelete = $('.action > a:nth-child(2)');

        //Check table head values

        for (let i = 0; i < tableHead.length; i++) {
            assert.equal(arrTableHead[i], tableHead[i], 'Table head name is incorrect');
        };

        //check each row value

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ')').getText().split(' ');
            for (let j = 0; j < arrTableBody.length; j++) {
                assert.equal(changingRow[j], arrAll[i - 1][j], 'Value in table row #' + i + ' is incorrect');
            };
        };

        //check links

        btnLinkEdit.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/tables#edit', 'Browser URL after button click is wrong');
        btnLinkDelete.click();
        assert.equal(browser.getUrl(), 'http://the-internet.herokuapp.com/tables#delete', 'Browser URL after button click is wrong');

        // check table values after sort

        const arrSortLastName = [];
        const arrSortFirstName = [];
        const arrSortEmailName = [];
        const arrSortDue = [];
        const arrSortWebSite = [];
        let arrNewSort = [];

        //get values from column "Last name" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.last-name').getText();
            arrSortLastName.push(changingRow);
            arrSortLastName.sort();
        };

        //check sorting function in column "Last name"

        $('#table2 > thead > tr > th:nth-child(1)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.last-name').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortLastName[i - 1], arrNewSort[i - 1], 'Sorting in column "Last name" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "First name" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.first-name').getText();
            arrSortFirstName.push(changingRow);
            arrSortFirstName.sort();
        };

        //check sorting function in column "First name"

        $('#table2 > thead > tr > th:nth-child(2)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.first-name').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortFirstName[i - 1], arrNewSort[i - 1], 'Sorting in column "First name" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Email" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.email').getText();
            arrSortEmailName.push(changingRow);
            arrSortEmailName.sort();
        };

        //check sorting function in column "Email"

        $('#table2 > thead > tr > th:nth-child(3)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.email').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortEmailName[i - 1], arrNewSort[i - 1], 'Sorting in column "Email" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Due" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.dues').getText().replace('$', '');
            arrSortDue.push(changingRow);
            arrSortDue.sort((a, b) => a - b);
        };

        //check sorting function in column "Due"

        $('#table2 > thead > tr > th:nth-child(4)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.dues').getText().replace('$', '');
            arrNewSort.push(changingRow);
            assert.equal(arrSortDue[i - 1], arrNewSort[i - 1], 'Sorting in column "Due" is not working');
        };
        arrNewSort.splice(0, arrNewSort.length); //clear array values

        //get values from column "Web Site" and sort them

        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.web-site').getText();
            arrSortWebSite.push(changingRow);
            arrSortWebSite.sort();
        };

        //check sorting function in column "Web Site"

        $('#table2 > thead > tr > th:nth-child(5)').click();
        for (let i = 1; i <= 4; i++) {
            let changingRow = $('#table2 > tbody > tr:nth-child(' + i + ') > td.web-site').getText();
            arrNewSort.push(changingRow);
            assert.equal(arrSortWebSite[i - 1], arrNewSort[i - 1], 'Sorting in column "Web Site" is not working');
        };

    });
});

describe('Status Codes', () => {
    it('Test server status answer', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Status Codes').click();
        const link = $('#content > div > p:nth-child(2) > a');
        const linkUrl = link.getAttribute('href');
        const content = $('#content > div > p');
        let xhr = new XMLHttpRequest();
        assert.equal(link.waitForDisplayed(), true, 'Redirection link is unavailable');
        assert.notEqual(content.getText().indexOf('For a complete list of status codes, go here.'), '-1', 'Content text is incorrect');

        link.click();
        assert.equal(browser.getUrl(), linkUrl, 'Link for a complete list of status codes is unavailable');
        browser.url('https://the-internet.herokuapp.com/status_codes');

        //check redirection page
        const linkStatusArr = $('#content > div > ul').getText().split('\n');
        const arr = '200,301,404,500';
        const linkList = arr.split(',');

        for (let i = 0; i < 4; i++) {
            //check server status list
            assert.equal(linkStatusArr[i], linkList[i], 'Status list text is incorrect');

            //check server status answer each link
            xhr.open('GET', $('#content > div > ul > li:nth-child(' + (i + 1) + ') > a').getAttribute('href'), false);
            xhr.send();
            assert.equal(xhr.readyState, '4', 'Query status is incorrect');
            assert.equal(xhr.status, linkStatusArr[i], 'This page do not return a ' + linkStatusArr[i] + ' status code.');

            //check content text and go back to parent page
            $('[href="status_codes/' + linkStatusArr[i] + '"]').click();
            assert.notEqual($('#content > div > p').getText().indexOf('This page returned a ' + linkStatusArr[i] + ' status code.'), '-1', 'Content text on "Status Codes" page is incorrect');
            $('[href="/status_codes"]').waitForDisplayed();
            $('[href="/status_codes"]').click();
        };

    });
});

describe('Typos', () => {
    it('Test typos visibility', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=Typos').click();
        const content = $('.example');
        const loadTypo = $('#content > div > p:nth-child(3)');
        content.waitForDisplayed();

        try {
            while (loadTypo.getText() === 'Sometimes you\'ll see a typo, other times you won\'t.') {
                browser.refresh();
                content.waitForDisplayed();
                console.log(loadTypo.getText());
            };
            if (loadTypo.getText() === 'Sometimes you\'ll see a typo, other times you won,t.') {
                throw new SyntaxError("Here is some typo!!!");
            };
        } catch (error) {
            console.log('!!!' + error);
        };

    });
});

describe('WYSIWYG Editor', () => {
    it('Test WYSIWYG Editor', () => {
        browser.url('http://the-internet.herokuapp.com');
        $('a=WYSIWYG Editor').click();
        $('#mceu_13').waitForDisplayed();
        assert.strictEqual($('#content > div > h3').getText(), 'An iFrame containing the TinyMCE WYSIWYG Editor');

        //check input field

        browser.switchToFrame('mce_0_ifr')
        const inputField = $('#tinymce > p');
        assert.strictEqual(inputField.getText(), 'Your content goes here.', 'Can\'t reach input field');
        $('#tinymce > p').clearValue();
        assert.strictEqual(inputField.getText(), '', 'Input field is not empty');
        $('#tinymce').setValue('Hello World!');
        assert.strictEqual(inputField.getText(), 'Hello World!', 'Can\'t input new value');
        browser.switchToParentFrame();

        //check menu button - File

        $('#mceu_15-open').click();
        $('#mceu_32').waitForDisplayed();
        $('#mceu_32').click();
        browser.switchToFrame('mce_0_ifr')
        assert.strictEqual(inputField.getText(), '', 'Input field is not empty');
        browser.switchToParentFrame();

        //check Align center function

        browser.switchToFrame('mce_0_ifr')
        $('#tinymce').setValue('Hello World!');
        assert.strictEqual(inputField.getText(), 'Hello World!', 'Can\'t input new value');
        browser.switchToParentFrame();
        $('#mceu_6 > button').click();
        browser.switchToFrame('mce_0_ifr')
        assert.equal(inputField.getAttribute('data-mce-style'), 'text-align: center;', 'Text formatting is not working');
        browser.switchToParentFrame();

        browser.refresh();

        //check Undo function

        $('#mceu_2-open').click();
        $('#mceu_32').waitForDisplayed();
        $('#mceu_32').click();
        $('#mceu_38').waitForDisplayed();
        $('#mceu_38').click();
        browser.switchToFrame('mce_0_ifr');
        assert.equal($('#tinymce > h2').getTagName(), 'h2', 'Text formatting is not working');
        browser.switchToParentFrame();
        $('#mceu_0 > button').click();
        browser.switchToFrame('mce_0_ifr');
        assert.equal($('#tinymce > p').getTagName(), 'p', 'Undo changes is not working');
        browser.switchToParentFrame();

        //check menu button click (deep version)
        browser.refresh();
        $('#mceu_18-open').click();
        $('#mceu_39').waitForDisplayed();
        $('#mceu_39').click();
        $('#mceu_43').waitForDisplayed();
        $('#mceu_43').click();
        $('#mceu_53').waitForDisplayed();
        $('#mceu_53').click();
        browser.switchToFrame('mce_0_ifr');
        assert.equal($('#tinymce > h6').getTagName(), 'h6', 'Text formatting is not working');
        browser.switchToParentFrame();

    });
});